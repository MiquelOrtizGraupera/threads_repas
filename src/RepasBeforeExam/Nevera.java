package RepasBeforeExam;

public class Nevera {
    int usuaris;
    int cervezas;
    boolean falten;


    public Nevera(int usuaris) {
        this.usuaris = usuaris;
        this.cervezas=6;
        this.falten=false;
    }

    protected synchronized void afegeixCervesa(){
        faltenBirres();
        if(!falten){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+ " no falten cerveses");
        }
        else {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int add = 6 - cervezas;
            cervezas = cervezas+add;
            System.out.println(Thread.currentThread().getName()+ " ha afegit "+add+ " cervezas");
        }
    }

    protected synchronized void beuCervesa(){
        if(cervezas==0){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("NO QUEDEN CERVESES!");
        }else if(cervezas==2){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " veu les últimes 2 birres");
            cervezas = cervezas -2;
        }else{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cervezas = cervezas -2;
            System.out.println(Thread.currentThread().getName()+" beu birra queden "+cervezas+" cerveses");
        }
    }

    private void faltenBirres(){
        falten = cervezas != 6;
    }


}

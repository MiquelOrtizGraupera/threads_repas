package RepasBeforeExam;


public class Main{
    public static void main(String[] args) throws InterruptedException{
        /*Thread t1=new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2=new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3=new Thread(new PrintThread("En un lugar de la Mancha"));
        t1.start();
        t2.start();
        t3.start();*/

        Nevera nevera=new Nevera(4);

        Thread t1=new Thread(()->nevera.afegeixCervesa(),"Pepi");
        Thread t2=new Thread(()->nevera.beuCervesa(),"Luci");
        Thread t3=new Thread(()->nevera.beuCervesa(),"Bom");
        Thread t4=new Thread(()->nevera.beuCervesa(),"Anna");

        t1.start();
        t2.start();
        t3.start();
        t4.start();


    }
}

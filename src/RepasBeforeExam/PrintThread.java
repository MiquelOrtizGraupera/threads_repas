package RepasBeforeExam;

import java.util.Scanner;

public class PrintThread implements Runnable{
    String frase;

    public PrintThread(String frase) {
        this.frase = frase;
    }

    private static synchronized void mostrar(String s){
        try (Scanner lector = new Scanner(s)) {

            while(lector.hasNext()){
                String word = lector.next();
                Thread.sleep(500);
                System.out.print(word+" ");
            }
            System.out.println();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
      mostrar(frase);
    }

}

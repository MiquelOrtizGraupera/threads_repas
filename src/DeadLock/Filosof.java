package DeadLock;

public class Filosof extends Thread{
    private final String nom;
    private final Objecte a;
    private final Objecte b;

    public Filosof(String nom, Objecte a, Objecte b) {
        this.nom = nom;
        this.a = a;
        this.b = b;
    }
    @Override
    public void run() {
        synchronized (a) {

            try{
                Thread.sleep(1000);
                System.out.println(nom + " agafa primer recurs " + a.getTipus());

            }catch(InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (b) {

                try{
                    Thread.sleep(1000);
                    System.out.println(nom + " agafa segon recurs " + b.getTipus());
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
            System.out.println(nom + " deixa " + b.getTipus());
        }
        System.out.println(nom + " deixa " + a.getTipus());
    }

}

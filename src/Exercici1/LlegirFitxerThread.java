package Exercici1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class LlegirFitxerThread implements Runnable {
    String ruta;

    public LlegirFitxerThread(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public void run() {
            Path path = Path.of(ruta);

            try(Scanner lector = new Scanner(path)){

                if(Files.exists(path)){
                    int numeroLinies = 0;
                    while(lector.hasNextLine()){
                        Thread.sleep(1500);
                        numeroLinies++;
                        String linia = lector.nextLine();
                        System.out.println("Llegint "+linia);
                    }
                    System.out.println("Linies totals de la ruta: "+numeroLinies);
                }else{
                    System.err.println("Ruta mal escrita");
                }
            }catch (InterruptedException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
    }

    public static void main(String[] args) {
        Thread primer = new Thread(new LlegirFitxerThread("C:\\Users\\Miquel\\IdeaProjects\\ThreadsUF2_Repas\\src\\Exercici1\\perObrir.txt"));
        Thread segon = new Thread(new LlegirFitxerThread("C:\\Users\\Miquel\\IdeaProjects\\ThreadsUF2_Repas\\src\\Exercici1\\AltreFitxer.txt"));

        primer.start();
        segon.start();
    }
}

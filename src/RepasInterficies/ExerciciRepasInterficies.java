package RepasInterficies;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Persona {
    private String nom;
    private int edat;

    public Persona(String nom, int edat) {
        this.nom = nom;
        this.edat = edat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nom='" + nom + '\'' +
                ", edat=" + edat +
                '}';
    }


}


public class ExerciciRepasInterficies {
    public static void main(String[] args) {
        ArrayList<Persona> llistaPersona = new ArrayList<>();
        llistaPersona.add(new Persona("Miquel",34));
        llistaPersona.add(new Persona("Marta",35));
        llistaPersona.add(new Persona("Jordi",63));
        llistaPersona.add(new Persona("Kintaso",50));

       ArrayList<Integer> llistaNumeros = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int numero = (int) (Math.random()*11);
            llistaNumeros.add(numero);
        }
        System.out.println(llistaNumeros);
        Collections.sort(llistaNumeros);
        System.out.println(llistaNumeros);

        //Comparing by Age
        System.out.println(llistaPersona);
        llistaPersona.sort(Comparator.comparingInt(Persona::getEdat));
        System.out.println(llistaPersona);
        //Comparing by name
        llistaPersona.sort(Comparator.comparingInt(p -> p.getNom().length()));
        System.out.println(llistaPersona);

    }



}

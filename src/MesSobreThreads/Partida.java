package MesSobreThreads;

import java.io.IOException;

public class Partida {
    public static void main(String[] args) throws  InterruptedException {
            Jugador primer = new Jugador("Joan");
            Jugador segon = new Jugador("Miquel");
            Jugador tercer = new Jugador("Pere");

            Thread t1 = new Thread(primer);
            Thread t2 = new Thread(segon);
            Thread t3 = new Thread(tercer);

            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();

    }
}

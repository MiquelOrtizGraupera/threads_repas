package MesSobreThreads;

import java.util.ArrayList;
import java.util.Comparator;

public class Jugador implements Runnable {
    String nom;

    public Jugador(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public void run() {

            try {
                System.out.println("Tira el jugador: "+nom);
                for (int i = 0; i < 3; i++) {
                    System.out.println("Tirada numero: " + i);
                    Thread.sleep(1000);
                    int numero = (int) (Math.random() * 6 + 1);
                    System.out.println("El numero de dau es: " + numero);
                }
                System.out.println("#####FIN TIRADA#####");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }
    @Override
    public String toString() {
        return "Jugador{" +
                "nom='" + nom + '\'' +
                '}';
    }
}
